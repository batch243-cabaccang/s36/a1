const express = require("express");
const taskRouter = express.Router();

const taskControllers = require("../controllers/taskController");

// GET All
taskRouter.get("/", taskControllers.getAll);

// Get One
taskRouter.get("/:id", taskControllers.getTaskId, taskControllers.getOneTask);

// PUT One
taskRouter.post("/", taskControllers.createTask);

// UPDATE One
taskRouter.put("/:id/complete", taskControllers.getTaskId, taskControllers.updateTask);

// DELETE One
taskRouter.delete(
  "/:id",
  taskControllers.getTaskId,
  taskControllers.deleteTask
);

module.exports = taskRouter;
