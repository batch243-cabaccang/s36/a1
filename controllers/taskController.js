const Task = require("../models/taskModel");

const taskControllers = {
  // GET All
  getAll: async (req, res) => {
    try {
      const allTasks = await Task.find();
      res.status(200);
      res.json(allTasks);
    } catch (error) {
      res.status(500);
      res.json({ message: error.message });
    }
  },

  // GET One
  getOneTask: async (req, res) => {
    const theTask = await res.task;
    res.json(theTask);
  },

  createTask: async (req, res) => {
    const taskDetails = new Task({
      name: req.body.name,
    });
    try {
      const newTask = await taskDetails.save();
      res.status(201);
      res.json(newTask);
    } catch (error) {
      res.status(402);
      res.json({ message: error.message });
    }
  },

  // UPDATE
  updateTask: async (req, res) => {
    if (req.body.name) {
      res.task.name = req.body.name;
    }
    if (req.body.status) {
      res.task.status = req.body.status;
    }
    try {
      const updatedTask = await res.task.save();
      res.status(200);
      res.json(updatedTask);
    } catch (error) {
      res.status(400);
      res.json({ message: error.message });
    }
  },

  // DELETE Task
  deleteTask: async (req, res) => {
    try {
      await res.task.remove();
      res.status(200);
      res.json({ message: `Task with ID ${res.task.id} has been deleted.` });
    } catch (error) {
      res.status(500);
      res.json({ message: error.message });
    }
  },

  getTaskId: async (req, res, next) => {
    let task;
    try {
      task = await Task.findById(req.params.id);
      if (!task) {
        res.status(404);
        res.json({ message: "Task Not Found" });
        return;
      }
    } catch (error) {
      res.status(500);
      res.json({ message: error.message });
    }
    res.task = task;
    next();
  },
};

module.exports = taskControllers;
