require("dotenv").config();

const express = require("express");
const app = express();
const port = process.env.PORT;
const mongoose = require("mongoose");

mongoose.connect(process.env.DB_CONNECTION);
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => {
  console.log("Connected to DB");
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const taskRouter = require("./routes/taskRoutes");
app.use("/tasks", taskRouter);

app.listen(port, console.log(`Connected to Port ${port}`));
